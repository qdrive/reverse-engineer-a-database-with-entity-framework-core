# Reverse Engineer a Database with Entity Framework Core

This example will demonstrate how to generate some **POCO** (Plain Old CLR Objects) and **DBContext** classes by reverse engineering an MSSQL database. This example uses the .NET Core v2.1.202 SDK which can be downloaded from [here](https://www.microsoft.com/net/download). Please note that this is an **example** and extra care and attention should be used if targetting a production database. 

## The Example Database

Connect to an MSSQL server instance and execute this script to create a simple database with 2 tables.

``` sql
CREATE DATABASE People
GO
USE People
CREATE TABLE dbo.Person
(
	PersonId INT IDENTITY PRIMARY KEY,
	Forename VARCHAR(255),
	Surname VARCHAR(255),
);

CREATE TABLE dbo.PersonInfo
(
	EmailId INT PRIMARY KEY,
	EmailAddress VARCHAR(255),
	PersonId INT UNIQUE FOREIGN KEY REFERENCES dbo.Person(PersonId)
);
```

## Build the Application

Create a new directory for your project then run the following command to create a new console application:

```
dotnet new console -n "MyApp"
```

Upon completion, navigate to the root of the application and add the required packages by editing **MyApp.csproj** and include the following values between the ``<Project></Project>`` elements:

``` xml
  <ItemGroup>
    <PackageReference Include="Microsoft.AspNetCore" Version="2.1.1" />
    <PackageReference Include="Microsoft.AspNetCore.Mvc" Version="2.1.1" />
    <PackageReference Include="Microsoft.EntityFrameworkCore.Design" Version="2.1.1" />
    <PackageReference Include="Microsoft.EntityFrameworkCore.SqlServer" Version="2.1.1" />
  </ItemGroup>

  <ItemGroup>
    <DotNetCliToolReference Include="Microsoft.VisualStudio.Web.CodeGeneration.Tools" Version="2.0.4" />
    <DotNetCliToolReference Include="Microsoft.EntityFrameworkCore.Tools.DotNet" Version="2.0.3" />
  </ItemGroup>
```

Run the following command to fetch the packages and build the project:

```
dotnet restore
```

Make sure the the Entity Framework CLI is working by running the following command:

```
dotnet ef
```

A successfull response will return something similar to this:

``` ps1
                     _/\__
               ---==/    \\
         ___  ___   |.    \|\
        | __|| __|  |  )   \\\
        | _| | _|   \_/ |  //|\\
        |___||_|       /   \\\/\\

Entity Framework Core .NET Command Line Tools 2.0.3-rtm-10026
```

## Generate the POCO's

Within the root of the application, run the following command to connect to the database and build the classes:

> The `-c` switch determines the class name of the context file and the `-o` switch sets the output location of the generated files.

```
dotnet ef dbcontext scaffold "Server=<DATABASE-HOST>\<DATABASE-INSTANCE>;Database=People;Integrated Security=true;" Microsoft.EntityFrameworkCore.SqlServer -c "PeopleContext" -o "Output"
```

> Currently this process does not support the output of any tables without a **Primary Key**. Any such tables will be ignored by the CLI.

Once completed, the **Output** directory will contain 3 new classes. **PeopleContext.cs** provides database connection information and details about the schema. While **Person.cs** and **PersonInfo.cs** expose the schemas as POCO's which can be consumed in an application/library.

### PeopleContext.cs

``` cs
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MyApp.Output
{
    public partial class PeopleContext : DbContext
    {
        public PeopleContext()
        {
        }

        public PeopleContext(DbContextOptions<PeopleContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<PersonInfo> PersonInfo { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(
                  "Server=<DATABASE-HOST>\\<DATABASE-INSTANCE>;Database=People;Integrated Security=true;"
                );
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>(entity =>
            {
                entity.Property(e => e.Forename)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Surname)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PersonInfo>(entity =>
            {
                entity.HasKey(e => e.EmailId);

                entity.HasIndex(e => e.PersonId)
                    .HasName("UQ__PersonIn__AA2FFBE486EE2AAA")
                    .IsUnique();

                entity.Property(e => e.EmailId).ValueGeneratedNever();

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Person)
                    .WithOne(p => p.PersonInfo)
                    .HasForeignKey<PersonInfo>(d => d.PersonId)
                    .HasConstraintName("FK__PersonInf__Perso__3A81B327");
            });
        }
    }
}
```

### Person.cs

``` cs
using System;
using System.Collections.Generic;

namespace MyApp.Output
{
    public partial class Person
    {
        public int PersonId { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }

        public PersonInfo PersonInfo { get; set; }
    }
}
```

### PersonInfo.cs

``` cs
using System;
using System.Collections.Generic;

namespace MyApp.Output
{
    public partial class PersonInfo
    {
        public int EmailId { get; set; }
        public string EmailAddress { get; set; }
        public int? PersonId { get; set; }

        public Person Person { get; set; }
    }
}
```

These objects are now ready to be copied and consumed to any of your .NET Core projects without the need to manually create them. 