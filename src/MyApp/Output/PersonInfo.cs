﻿using System;
using System.Collections.Generic;

namespace MyApp.Output
{
    public partial class PersonInfo
    {
        public int EmailId { get; set; }
        public string EmailAddress { get; set; }
        public int? PersonId { get; set; }

        public Person Person { get; set; }
    }
}
