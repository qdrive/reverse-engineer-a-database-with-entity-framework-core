﻿using System;
using System.Collections.Generic;

namespace MyApp.Output
{
    public partial class Person
    {
        public int PersonId { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }

        public PersonInfo PersonInfo { get; set; }
    }
}
