﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MyApp.Output
{
    public partial class PeopleContext : DbContext
    {
        public PeopleContext()
        {
        }

        public PeopleContext(DbContextOptions<PeopleContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<PersonInfo> PersonInfo { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=<DATABASE-SERVER>\\<DATABASE-INSTANCE>;Database=People;Integrated Security=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>(entity =>
            {
                entity.Property(e => e.Forename)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Surname)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PersonInfo>(entity =>
            {
                entity.HasKey(e => e.EmailId);

                entity.HasIndex(e => e.PersonId)
                    .HasName("UQ__PersonIn__AA2FFBE486EE2AAA")
                    .IsUnique();

                entity.Property(e => e.EmailId).ValueGeneratedNever();

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Person)
                    .WithOne(p => p.PersonInfo)
                    .HasForeignKey<PersonInfo>(d => d.PersonId)
                    .HasConstraintName("FK__PersonInf__Perso__3A81B327");
            });
        }
    }
}
